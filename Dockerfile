FROM archlinux/base:latest

MAINTAINER Doug Davis <ddavis@cern.ch>

RUN pacman -Syu --noconfirm
RUN pacman -S doxygen python python-pip graphviz make --noconfirm

RUN pip install pip -U
RUN pip install sphinx>=2.0 mkdocs mkdocs-material pygments pymdown-extensions breathe sphinx-nameko-theme sphinx_rtd_theme m2r

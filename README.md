## Synopsis

Build a Docker image containing cutting edge tools (because we build
this image from Arch Linux) for documentating code (package includes
the name Doxygen because that was the original tool, but now it's
grown).

Supported tools:

- Doxygen
- Sphinx (with breathe and sphinx_rtd_theme)
- mkdocs
